class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
    @ships = @board.counts_all[:x]
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won? || @board.full?
  end

  def play_turn
    mov = @player.get_play
    if @board.empty?(mov)
      self.attack(mov)
    elsif @board[mov] == :s
      puts "Ship Sunk"
      @board[mov] = :x
    end
  end

end
