class Board

  attr_accessor :grid

  def initialize(board = nil)
    if board
      @grid = board
    else
      @grid = self.class.default_grid
    end
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  MARKER_HASH = {
    :s => " ",
    nil => " ",
    :x => "x"
  }

  def count
    self.counts_all[:s]
  end

  def counts_all
    counter = Hash.new(0)
    @grid.each do |row|
      row.each do |ch|
        counter[ch] += 1
      end
    end
    counter
  end

  def empty?(pos = nil)
    if pos
      self[pos] == nil
    elsif self.count == 0
      true
    else
      false
    end
  end

  def full?
    self.counts_all[nil] == 0
  end

  def place_random_ship
    raise "Error, board is full" if self.full?
    i = @grid.length
    row = (0...i).to_a.sample
    col = (0...i).to_a.sample
    pos = [row, col]
    self[pos] = :s
  end

  def won?
    self.counts_all[:s] == 0
  end
  
end
